# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=xfce4-screensaver
pkgver=4.16.0
pkgrel=7
pkgdesc="Screensaver and locker for XFCE"
url="https://gitlab.xfce.org/apps/xfce4-screensaver"
arch="all"
license="GPL-2.0-or-later"
depends="python3"
makedepends="
	dbus-glib-dev
	garcon-dev
	glib-dev
	gtk+3.0-dev
	libwnck3-dev
	libx11-dev
	libxext-dev
	libxfce4ui-dev
	libxklavier-dev
	libxrandr-dev
	libxscrnsaver-dev
	linux-pam-dev
	xfconf-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
source="https://archive.xfce.org/src/apps/xfce4-screensaver/${pkgver%.*}/xfce4-screensaver-$pkgver.tar.bz2
	$pkgname-glib2.76.patch::https://gitlab.xfce.org/apps/xfce4-screensaver/-/commit/7aeced1f6fb39fd8887fc441b4ff491ba5bfcf35.patch
	pam-base-auth.patch
	"
options="suid"
_libexecdir=/usr/lib/xfce4

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--libexecdir=$_libexecdir \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-pam \
		--enable-locking
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
5fa0381395b48fdfb5bdd9b4cafe1ec625d0b7fb9600d59c22c42fe0248fb4b99dd18a94045df3ecdc77635f7271676329c658f25003a9d8d6f9a9a66739dbe9  xfce4-screensaver-4.16.0.tar.bz2
dc9e289aa91ea4a5b65d2381bfabdbb48a6b07c67782afc53b003ffd2a07c0d225f4bb2ea48bcd478f14ab4c29a2521a0436085b40589e5a059e5561f7bcb42d  xfce4-screensaver-glib2.76.patch
0d53a0e9ee4b8bc5469b7b46068c614d188bb13dfc7d79565d61fae4c854dd4edc72ad7a785d09fb256d9e98564fa2325a2f8af7dbccad645fded5ed525d95ad  pam-base-auth.patch
"
