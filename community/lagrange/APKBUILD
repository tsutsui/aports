# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=lagrange
pkgver=1.15.5
pkgrel=0
pkgdesc="Beautiful Gemini client"
url="https://gmi.skyjake.fi/lagrange"
license="BSD-2-Clause"
arch="all"
makedepends="
	cmake
	fribidi-dev
	harfbuzz-dev
	libunistring-dev
	libwebp-dev
	mpg123-dev
	openssl-dev
	pcre2-dev
	samurai
	sdl2-dev
	zip
	zlib-dev
	"
subpackages="$pkgname-dbg $pkgname-doc"
source="https://git.skyjake.fi/gemini/lagrange/releases/download/v$pkgver/lagrange-$pkgver.tar.gz"
options="!check" # no test suite

[ "$CARCH" = "riscv64" ] && options="$options textrels"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_POPUP_MENUS=OFF \
		-DENABLE_RESIZE_DRAW=OFF \
		-DENABLE_X11_XLIB=OFF
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
be1a8669a54fdb09f510a73245a847b96dcdbe6359683ef89a96e9bb63a26534655598c8c7b51893d127c84621a66cedb3d835eed8e2de496b8130e8141d487c  lagrange-1.15.5.tar.gz
"
