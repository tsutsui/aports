# Contributor: Grigory Kirillov <txgk@bk.ru>
# Maintainer: Grigory Kirillov <txgk@bk.ru>
pkgname=simdjson
pkgver=3.1.6
pkgrel=0
pkgdesc="Parsing gigabytes of JSON per second"
url="https://simdjson.org"
arch="all"
license="Apache-2.0"
# tests take a very long time to compile and require downloading other JSON parsers
options="!check"
makedepends="cmake samurai"
subpackages="$pkgname-static $pkgname-dev"
source="https://github.com/simdjson/simdjson/archive/v$pkgver/simdjson-$pkgver.tar.gz"

build() {
	cmake -B builddir -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DBUILD_SHARED_LIBS=ON
	cmake -B builddir-static -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release
	cmake --build builddir
	cmake --build builddir-static
}

package() {
	DESTDIR="$pkgdir" cmake --install builddir
	DESTDIR="$pkgdir" cmake --install builddir-static
}

sha512sums="
72f27b010e659025f9c8842daf79364d0d0f40cddd66858956ab4fa4f3f3a631fe342f440201d58ed9af42a4356aafafaac8d3caf3317dd1a6314dad3a71081a  simdjson-3.1.6.tar.gz
"
